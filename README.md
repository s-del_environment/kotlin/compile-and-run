# コンパイルと実行
Windows 環境でコンパイルと実行


## kotlinc
### Java クラスファイル
```ps1
# コンパイル
kotlinc .\Main.kt

# 実行
kotlin .\MainKt.class
```
### jar ファイル
```ps1
# コンパイル
kotlinc .\Main.kt -include-runtime -d .\Main.jar

# 実行
kotlin .\Main.jar
# or
java -jar .\Main.jar
```


## IntelliJ IDEA
- ビルド: <kbd>Ctrl</kbd>+<kbd>F9</kbd>
- ビルドと実行: <kbd>Ctrl</kbd>+<kbd>F10</kbd>
### jar ファイル
プロジェクトのビルドシステムに Gradle を選んだ場合
```ps1
# コンパイル
.\gradlew jar

# 実行
# -classpath オプションで jar を、パッケージ名を含めたエントリポイントも指定。
kotlin -classpath .\build\libs\<NAME>.jar <PACKAGE>.MainKt
```
